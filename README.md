# C# Selenium using Specflow Automation Test Framework #

This is a C# Web and API automation test framework powered by Selenium and SpecFlow.

### Tools and Programming language Used ###

* C# as a programming language
* Specflow as Behavior driven testing tool
* Rest Sharp for API testing
* Extent Report for Reporting

### Project Directory Structure ###

![Optional Text](DirStructure.PNG)

### Environment Setup ###

* Install Visual Studio 2019
* Install .net Core SDK 3.1

### How do I get set up and exeute specflow files ? ###

* Clone project from Bitbucket (https://bitbucket.org/devsolutions009/gametwistc/src/master/)
* Open project in Visual Studio
* Add Specflow plugin in Visual Studio
* Add require Dependencies in project
* Build the Project
* Run Specflow Feature file
* View the result in index.html file in Reports folder
* View the Screenshot captured in Screenshot folder

### Extent Reporting ###
* Pass Report

![Optional Text](Pass_Test_Report.png)

* Failure Report

![Optional Text](Fail_Test_Report.png)

