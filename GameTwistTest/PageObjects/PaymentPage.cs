﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameTwistTest.PageObjects {
    public class PaymentPage {
        public IWebDriver WebDriver { get; }

        public PaymentPage(IWebDriver webDriver) {
            WebDriver = webDriver;
        }

        public IWebElement nextButton => WebDriver.FindElement(By.XPath("//button//span[text()='Next']"));
        public IWebElement bankDropdown => WebDriver.FindElement(By.XPath("//select[@id='epsIssuerSelect']"));
        public IWebElement submitButton => WebDriver.FindElement(By.XPath("//input[@id='mainSubmit']"));
        public IWebElement username => WebDriver.FindElement(By.XPath("//input[@name='gebForm:verf_ID']"));
        public IWebElement password => WebDriver.FindElement(By.XPath("//input[@name='gebForm:pin_ID']"));
        public IWebElement loginButton => WebDriver.FindElement(By.XPath("//input[@name='gebForm:LoginCommandButton']"));
        public IWebElement errorMessage => WebDriver.FindElement(By.XPath("//div[contains(@class,'messagebox_error')]//div[contains(@class,'messagebox_topic')]//span"));
        public IWebElement cancelButtonPaymentPage => WebDriver.FindElement(By.XPath("//ul//li//a[@id='gebForm:j_id71']"));
        public IWebElement cancelButton => WebDriver.FindElement(By.XPath("//div//a[@id='gebForm:j_id74']"));

        public void clickOnNextButton() {
            new WebDriverWait(WebDriver, TimeSpan.FromSeconds(60)).
                Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//button//span[text()='Next']"))).Click();
        }

        public void selectBankFromDropdown(object bankName) {
            SelectElement select = new SelectElement(bankDropdown);
            select.SelectByText(bankName.ToString());
        }

        public void clickOnSubmitButton() => submitButton.Click();

        public void enterLoginDetails(object userName, object pin) {
            username.SendKeys(userName.ToString());
            password.SendKeys(pin.ToString());
            new WebDriverWait(WebDriver, TimeSpan.FromSeconds(60)).
                Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath("//input[@name='gebForm:LoginCommandButton']"))).Click();
        }

        public String ErrorMessages => new WebDriverWait(WebDriver, TimeSpan.FromSeconds(60)).
                Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//div[contains(@class,'messagebox_error')]//div[contains(@class,'messagebox_topic')]//span"))).Text.ToString();

        public void clickCancelButton() => cancelButtonPaymentPage.Click();

        public void navigateHome(String expectedUrl) {
            String getCurrentUrl = WebDriver.Url;
            if (!getCurrentUrl.Contains(expectedUrl)) {
                DefaultWait<IWebDriver> fluentWait = new DefaultWait<IWebDriver>(WebDriver);
                fluentWait.Timeout = TimeSpan.FromSeconds(60);
                fluentWait.PollingInterval = TimeSpan.FromMilliseconds(250);
                /* Ignore the exception -NoSuchElementException that indicates that the element is not present */
                fluentWait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                fluentWait.Until(x => x.FindElement(By.XPath("//h1[contains(text(),'EPS Mobile')]")));
                Actions actions = new Actions(WebDriver);
                actions.MoveToElement(cancelButton);
                actions.Perform();
                cancelButton.Click();
                fluentWait.Until(x => x.FindElement(By.XPath("//Header//div[contains(text(),'Login')]")));
            }
        }

        public String getCurrentUrl() {
            return WebDriver.Url;
        }
    }
}
