﻿using AventStack.ExtentReports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace GameTwistTest.Hooks
{
    [Binding]
    public sealed class GameTwistHooks {
        private readonly ScenarioContext _scenarioContext;
        ExtentReports rep = ExtentManager.GetInstance();
        ExtentTest test;

        public GameTwistHooks(ScenarioContext scenarioContext) {
            _scenarioContext = scenarioContext;

        }

        [BeforeScenario]
        public void BeforeScenario() {
            Console.WriteLine("In Method Before Scenario!");
            test = rep.CreateTest(_scenarioContext.ScenarioInfo.Title);
        }

        [AfterScenario]
        public void AfterScenario() {
            Console.WriteLine("In Method After Scenario!");
            rep.Flush();
        }

        [BeforeStep("test")]
        public void BeforeStep() {
            Console.WriteLine("In Method Before Step!");
        }

        [AfterStep]
        public void AfterStep() {
            if (_scenarioContext.ScenarioExecutionStatus.ToString().Equals("OK")) {               
                test.Log(Status.Pass, ScenarioStepContext.Current.StepInfo.Text);
            }
            else if (_scenarioContext.ScenarioExecutionStatus.ToString().Equals("TestError")) {
                var stacktrace = _scenarioContext.TestError.Message;
                test.Log(Status.Fail, stacktrace);
            }
        }

        [BeforeScenarioBlock]
        public void BeforeScenarioBLock() {
            Console.WriteLine("In Method Before Scenario block!");
        }

        [AfterScenarioBlock]
        public void AfterScenarioBlock() {
            Console.WriteLine("In Method After ScenarioBlock!");
        }

        [BeforeFeature]
        public static void BeforeFeature() {
            Console.WriteLine("In Method Before Feature!");
        }

        [AfterFeature]
        public static void AfterFeature() {
            Console.WriteLine("In Method After Feature!");
        }

        [BeforeTestRun]
        public static void BeforeTestRun() {
            Console.WriteLine("In Method Before test run!!");
        }

        [AfterTestRun]
        public static void AfterTestRun() {
            Console.WriteLine("In Method After test run!!");
        }
    }
}
