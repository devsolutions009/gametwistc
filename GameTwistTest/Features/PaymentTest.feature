﻿Feature: GameTwistPaymentFeature
 
@smoke
Scenario: A registered player should be able to make a purchase on Gametwist.com
Given I login using valid credential
When  I accept the consent
| consentType               |
| GeneralTermsAndConditions |
When I valdiate the consent
| consentType               |
| GeneralTermsAndConditions |
When I validate registration
Then I purchase the game
When I am on Payment Home Page
When I validates error message for invalid user details
| username  | pin   | bankname     |
| test_user | 99333 | Bank Austria |
Then I navigates back to home page
Then I capture screenshot and I close the browser