﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using RestSharp;
using System;
using System.Linq;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using Newtonsoft.Json.Linq;
using GameTwistTest.PageObjects;
using System.Threading;
using System.IO;
using GameTwistTest.Utils;

namespace GameTwistTest.Steps{
    [Binding]
    public class PaymentApi {
        private readonly ScenarioContext _scenarioContext;
        RestClient restClient = new RestClient(Constants.GAMETWIST_API_BASE_URL);

        public PaymentApi(ScenarioContext scenarioContext) {
            _scenarioContext = scenarioContext;
        }

        [Given(@"I login using valid credential")]
        public void validateLogin() {
            JObject jObjectbody = new JObject();
            jObjectbody.Add("nickname", "Testing7652");
            jObjectbody.Add("password", "Welcome_2_gametwist");
            jObjectbody.Add("autologin", true);
            RestRequest restRequest = new RestRequest("/login-v1", Method.POST);
            restRequest.AddHeader("Accept", "application/json");
            restRequest.AddHeader("charset", "utf-8");
            restRequest.AddParameter("application/json", jObjectbody, ParameterType.RequestBody);
            IRestResponse restResponse = restClient.Execute(restRequest);
            int StatusCode = (int)restResponse.StatusCode;
            string token = restResponse.Headers.ToList()
            .Find(x => x.Name == "x-nrgs-auth-token-jwt")
            .Value.ToString();
            _scenarioContext["JwtToken"] = token;
            Assert.AreEqual(200, StatusCode, "Status code is not 200");
        }

        [When(@"I accept the consent")]
        public void validateConsentType(Table table) {
            dynamic consent = table.CreateDynamicInstance();

            RestRequest restRequest = new RestRequest("/consent/consent-v1?consentType=" + consent.consentType + "&accepted=true", Method.POST);
            restRequest.AddHeader("Accept", "application/json");
            restRequest.AddHeader("charset", "utf-8");
            restRequest.AddHeader("Authorization", "Bearer " + _scenarioContext["JwtToken"]);
            IRestResponse restResponse = restClient.Execute(restRequest);
            int StatusCode = (int)restResponse.StatusCode;
            Assert.AreEqual(200, StatusCode, "Status code is not 200");
        }

        [When(@"I valdiate the consent")]
        public void getConsentType(Table table) {
            dynamic consent = table.CreateDynamicInstance();
            RestRequest restRequest = new RestRequest("/consent/consent-v1?consentType="+ consent.consentType, Method.GET);
            restRequest.AddHeader("Accept", "application/json");
            restRequest.AddHeader("charset", "utf-8");
            restRequest.AddHeader("Authorization", "Bearer " + _scenarioContext["JwtToken"]);
            IRestResponse restResponse = restClient.Execute(restRequest);
            int StatusCode = (int)restResponse.StatusCode;
            Assert.AreEqual(200, StatusCode, "Status code is not 200");
        }

        [When(@"I validate registration")]
        public void validateFullRegistration() {
            JObject jObjectbody = new JObject();
            jObjectbody.Add("firstName", "Johnathaa");
            jObjectbody.Add("lastName", "Doeya");
            jObjectbody.Add("isMale", "true");
            jObjectbody.Add("countryCode", "AT");
            jObjectbody.Add("city", "Vienna");
            jObjectbody.Add("street", "Wiedner Hauptstraße 94");
            jObjectbody.Add("zip", "1050");
            jObjectbody.Add("phonePrefix", "43");
            jObjectbody.Add("phoneNumber", "12345678");
            jObjectbody.Add("securityQuestionTag", "squestion_name_of_first_pet");
            jObjectbody.Add("securityAnswer", "Heena");
            RestRequest restRequest = new RestRequest("/player/upgradeToFullRegistrationGT-v1", Method.POST);
            restRequest.AddHeader("Accept", "application/json");
            restRequest.AddHeader("charset", "utf-8");
            restRequest.AddHeader("Authorization", "Bearer " + _scenarioContext["JwtToken"]);
            restRequest.AddParameter("application/json", jObjectbody, ParameterType.RequestBody);
            IRestResponse restResponse = restClient.Execute(restRequest);
            int StatusCode = (int)restResponse.StatusCode;
            if (StatusCode == 200) {
                Console.WriteLine("User is sucessfully Registered");
            } else if (StatusCode == 403) {
                var jObject = JObject.Parse(restResponse.Content);
                String messge = jObject.GetValue("message").ToString();
                Assert.AreEqual(messge, "Player already upgraded to full registration");
                String code = jObject.GetValue("code").ToString();
                Assert.AreEqual(code, "player-already-fully-registered");
            }
        }

        [Then(@"I purchase the game")]
        public void validatePurchase() {
            RestClient restClient = new RestClient(Constants.PAYMENT_API_BASE_URL);
            JObject jObjectbody = new JObject();
            jObjectbody.Add("item", "m");
            jObjectbody.Add("paymentTypeId", "adyenEPS");
            jObjectbody.Add("country", "AT");
            jObjectbody.Add("landingUrl", Constants.GAMETWIST_LANDING_URL);
            RestRequest restRequest = new RestRequest("/purchase-v1", Method.POST);
            restRequest.AddHeader("Accept", "application/json");
            restRequest.AddHeader("charset", "utf-8");
            restRequest.AddHeader("Authorization", "Bearer " + _scenarioContext["JwtToken"]);
            restRequest.AddParameter("application/json", jObjectbody, ParameterType.RequestBody);
            IRestResponse restResponse = restClient.Execute(restRequest);
            int StatusCode = (int)restResponse.StatusCode;
            Assert.AreEqual(200, StatusCode, "Status code is not 200");

            var jObject = JObject.Parse(restResponse.Content);
            string paymentUrl = jObject.GetValue("paymentRedirectUrl").ToString();
            _scenarioContext["paymentUrl"] = paymentUrl;
            Assert.AreEqual(200, StatusCode, "Status code is not 200");
        }
    }
}