﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using GameTwistTest.PageObjects;
using System.IO;
using GameTwistTest.Utils;

namespace GameTwistTest.Steps {
    [Binding]
    public class PaymentsWeb {
        private readonly ScenarioContext _scenarioContext;
        private readonly FeatureContext _featureContext;
        PaymentPage paymentpage = null;
        IWebDriver webDriver;

        public PaymentsWeb(ScenarioContext scenarioContext, FeatureContext featureContext) {
            _scenarioContext = scenarioContext;
            _featureContext = featureContext;
        }

        [When(@"I am on Payment Home Page")]
        public void ValidateNavigationToPaymentPage() {
            webDriver = new ChromeDriver();
            webDriver.Navigate().GoToUrl(_scenarioContext["paymentUrl"].ToString());
            webDriver.Manage().Window.Maximize();
            paymentpage = new PaymentPage(webDriver);
        }

        [When(@"I validates error message for invalid user details")]
        public void userEntersCredentialAndValidatesErrorMsg(Table table) {
            paymentpage.clickOnNextButton();
            dynamic bankName = table.CreateDynamicInstance();
            paymentpage.selectBankFromDropdown(bankName.bankname);
            paymentpage.clickOnSubmitButton();
            dynamic credentials = table.CreateDynamicInstance();
            paymentpage.enterLoginDetails(credentials.username, credentials.pin);
            String actualErrorMsg = paymentpage.ErrorMessages;
            Assert.AreEqual(actualErrorMsg, Constants.ERROR_MESSAGE);
        }

        [Then(@"I navigates back to home page")]
        public void userNavigatesBackToHomePageAndCaptureScreenshot() {
            paymentpage.clickCancelButton();
            paymentpage.navigateHome(Constants.GAMETWIST_HOME_URL);
            string actualUrl = paymentpage.getCurrentUrl();
            Assert.IsTrue(actualUrl.Contains(Constants.GAMETWIST_HOME_URL));
        }

        [Then(@"I capture screenshot and I close the browser")]
        public void CaptureScreenshotAndCloseTheBrowser() {
            //Capturing and saving the screenshot
            Screenshot image = ((ITakesScreenshot)webDriver).GetScreenshot();
            string workingDirectory = Environment.CurrentDirectory;
            string startupPath = Directory.GetParent(workingDirectory).Parent.Parent.FullName;
            image.SaveAsFile(startupPath + "/Screenshots/HomePage.png");
            if (webDriver != null) {
                webDriver.Dispose();
                webDriver = null;
            }
        }
    }
}