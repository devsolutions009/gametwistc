﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports.Reporter.Configuration;
using System;
using System.IO;
using TechTalk.SpecFlow;

namespace GameTwistTest
{
    [Binding]
    public class ExtentManager {
        public static ExtentHtmlReporter htmlReporter;
        private static ExtentReports extent;

        private ExtentManager() {

        }

        public static ExtentReports GetInstance() {
            if (extent == null) {
                string workingDirectory = Environment.CurrentDirectory;
                string startupPath = Directory.GetParent(workingDirectory).Parent.Parent.FullName;
                string reportpath = startupPath + "/Report/index.html";

                htmlReporter = new ExtentHtmlReporter(reportpath);
                htmlReporter.Config.Theme = Theme.Dark;

                extent = new ExtentReports();
                extent.AttachReporter(htmlReporter);
                extent.AddSystemInfo("Host Name", "GameTwist");
                extent.AddSystemInfo("Environment", "Test QA");
                extent.AddSystemInfo("Username", "Test_User");
                htmlReporter.LoadConfig(startupPath+"/ExtentConfig.xml"); //Get the config.xml file 
            }
            return extent;
        }
    }
}