﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameTwistTest.Utils {
    public static class Constants {
        public const string GAMETWIST_HOME_URL = "https://www.gametwist.com/en/";
        public const string  ERROR_MESSAGE = "Es sind Fehler aufgetreten!";
        public const string GAMETWIST_API_BASE_URL = "https://www.gametwist.com/nrgs/en/api";
        public const string PAYMENT_API_BASE_URL = "https://payments-api-v1-at.greentube.com/gametwist.widgets.web.site/en/api";
        public const string GAMETWIST_LANDING_URL = "https://www.gametwist.com/en/?modal=shop";
    }
}
